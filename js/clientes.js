var customers;

function getCustomers() {

  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";//?$filter=Country eq 'Germany'";

  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        console.log(request.responseText);

        customers = request.responseText;
        procesarCustomers();
      }
  }

  request.open("GET",url,true);
  request.send();

}

function procesarCustomers() {
  var JSONClientes = JSON.parse(customers);
  var divTabla = document.getElementById("tablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var newRow = document.createElement("tr");
    var nameColumn = document.createElement("td");
    nameColumn.innerText = JSONClientes.value[i].ContactName;
    var cityColumn = document.createElement("td");
    cityColumn.innerText = JSONClientes.value[i].City;
    var imgColumn = document.createElement("td");
    var img = document.createElement("IMG");
    if(JSONClientes.value[i].Country == "UK"){
      img.setAttribute("src", "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+"United-Kingdom"+".png");
    }else{
      img.setAttribute("src", "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png");
    }
    img.setAttribute("width", "70");
    img.setAttribute("height", "42");
    imgColumn.appendChild(img);

    newRow.appendChild(nameColumn);
    newRow.appendChild(cityColumn);
    newRow.appendChild(imgColumn);

    tbody.appendChild(newRow);
    //console.log(JSONClientes.value[i].ProductName);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
  //alert(JSONClientes.value[0].ProductName);
}
