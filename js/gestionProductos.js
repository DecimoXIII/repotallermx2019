var productosObtenidos;

function getProductos() {

  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";

  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        //console.log(request.responseText);
        productosObtenidos = request.responseText;
        procesarProductos();
      }
  }

  request.open("GET",url,true);
  request.send();

}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("tablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    var newRow = document.createElement("tr");
    var nameColumn = document.createElement("td");
    nameColumn.innerText = JSONProductos.value[i].ProductName;
    var priceColumn = document.createElement("td");
    priceColumn.innerText = JSONProductos.value[i].UnitPrice;
    var stockColumn = document.createElement("td");
    stockColumn.innerText = JSONProductos.value[i].UnitsInStock;
    newRow.appendChild(nameColumn);
    newRow.appendChild(priceColumn);
    newRow.appendChild(stockColumn);

    tbody.appendChild(newRow);
    //console.log(JSONProductos.value[i].ProductName);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
  //alert(JSONProductos.value[0].ProductName);
}
